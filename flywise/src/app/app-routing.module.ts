import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { LoginComponent } from './login/login.component';
import { ContactusComponent } from './contactus/contactus.component';
import { LogoutComponent } from './logout/logout.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { BookingsComponent } from './bookings/bookings.component';
import { FlightComponent } from './flight/flight.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: 'aboutus', component: AboutusComponent },
  { path: '', component: LoginComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'home', component: HomeComponent  },
  { path: 'register', component: RegisterComponent  },
  { path: 'fp', component: ForgotPasswordComponent  },
  { path: 'bookings', component: BookingsComponent  },
  { path: 'flights', component: FlightComponent  },
  { path: 'profile', component: ProfileComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
