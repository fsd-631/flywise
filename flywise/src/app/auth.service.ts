import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  isUserLoggedIn: boolean;
  // cartItems: any;
  loginStatus: any;
  constructor(private http: HttpClient) { 
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }
  registerUser(user: any): Observable<any> {
    return this.http.post('http://localhost:8080/user/signup', user);
  }
  forgotPassword(email: string):any {
    return this.http.post('http://localhost:8080/users/create', email);
  }
  userLogin(user: any): any {
    return this.http.post('http://localhost:8080/user/signin',user).toPromise;
  }
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

}
