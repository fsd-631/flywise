import { Component } from '@angular/core';
import emailjs, { EmailJSResponseStatus } from 'emailjs-com';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrl: './contactus.component.css'
})
export class ContactusComponent {
    constructor() {
    emailjs.init('bgKP-CeZpLJiMde-1'); 
  }

  sendEmail(e: Event) {
    e.preventDefault();
    emailjs.sendForm('service_m44yjqa', 'template_g7d7rnb', e.target as HTMLFormElement, 'bgKP-CeZpLJiMde-1')
      .then((response: EmailJSResponseStatus) => {
        console.log('Email sent successfully:', response);
        alert('Email sent successfully!');
        
        (document.getElementById('contactForm') as HTMLFormElement).reset();
      }, (error) => {
        console.error('Failed to send email:', error);
        alert('Failed to send email. Please try again later.');
      });
  }


}
