import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';  // Ensure you have AuthService set up for HTTP requests
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  message: string = '';  // Provide a default value

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    // Initialize forgotPasswordForm to avoid TS error
    this.forgotPasswordForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  ngOnInit(): void {}

  get email() {
    return this.forgotPasswordForm.get('email');
  }

  onSubmit(): void {
    if (this.forgotPasswordForm.valid) {
      const email = this.forgotPasswordForm.value.email;
      this.authService.forgotPassword(email).subscribe(
        (response: any) => {  // Explicitly type the response
          this.message = 'An email has been sent to reset your password.';
        },
        (error: any) => {  // Explicitly type the error
          this.message = 'Something went wrong. Please try again.';
        }
      );
    }
  }
}
