// import { Component, OnInit } from '@angular/core';
// import { AuthService } from '../auth.service';
// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrl: './register.component.css'
// })
// export class RegisterComponent {
//   User: any;
//   constructor(private service: AuthService) {
//     this.User = {
//       "Name": "",
//       "mobile": "",
//       "emailId": "",
//       "password": "",
      
//       }
//     }
//     registerSubmit(regForm: any) {
//       this.User.Name = regForm.Name;
      
//       // this.User.UserType = regForm.UserType;
      
//       this.User.mobile = regForm.mobile;
//       // this.employee.country = regForm.country;
//       this.User.emailId = regForm.emailId;
//       this.User.password = regForm.password;
//       // this.employee.department.deptId = regForm.deptId;
  
//       console.log(this.User);
//       this.service.registerUser(this.User).subscribe((data: any) => {
//         console.log(data);
//       });
//     }
  
  

// }
import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
 
  constructor(private service: AuthService) {
  }

  registerSubmit(regForm: any) {
    console.log(regForm);
    this.service.registerUser(regForm).subscribe((data: any) => {console.log(data);});
  }
}
